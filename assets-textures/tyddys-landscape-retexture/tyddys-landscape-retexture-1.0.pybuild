# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, InstallDir

LQ_SOURCE = "lq-textures_85902"
MQ_SOURCE = "mq-textures_25363"
HQ_SOURCE = "hq-textures_43301"
UHQ_SOURCE = "uhq-textures-use-with-hq_95710"


class Package(MW):
    NAME = "Tyddy's Landscape Retexture"
    DESC = "Retexture of soils, grass, stones and snow in Vvardenfell and Solstheim."
    HOMEPAGE = "https://www.fullrest.ru/files/tyd_landscape-texture_compilation_1"
    TEXTURE_SIZES = "512 1024 2048 4096"
    SRC_URI = f"""
        texture_size_512? ( https://www.fullrest.ru/uploads/files/{LQ_SOURCE}.7z )
        texture_size_1024? ( https://www.fullrest.ru/uploads/files/{MQ_SOURCE}.7z )
        texture_size_2048? ( https://www.fullrest.ru/uploads/files/{HQ_SOURCE}.7z )
        texture_size_4096? (
            https://www.fullrest.ru/uploads/files/{HQ_SOURCE}.7z
            https://www.fullrest.ru/uploads/files/{UHQ_SOURCE}.7z
        )
    """
    LICENSE = "all-rights-reserved"
    TIER = 1
    KEYWORDS = "openmw"
    RDEPEND = "base/morrowind[bloodmoon]"
    IUSE = "darkness"
    INSTALL_DIRS = [
        InstallDir(
            "LQ",
            BLACKLIST=["Extras"],
            REQUIRED_USE="texture_size_512",
            S=LQ_SOURCE,
        ),
        InstallDir(
            "MQ",
            BLACKLIST=["Extras"],
            REQUIRED_USE="texture_size_1024",
            S=MQ_SOURCE,
        ),
        InstallDir(
            "HQ",
            BLACKLIST=["Extras"],
            REQUIRED_USE="texture_size_2048",
            S=HQ_SOURCE,
        ),
        InstallDir(
            "HQ",
            BLACKLIST=["Extras"],
            REQUIRED_USE="texture_size_4096",
            S=HQ_SOURCE,
        ),
        InstallDir(
            "UHQ (use with HQ)",
            BLACKLIST=["Extras"],
            REQUIRED_USE="texture_size_4096",
            S=UHQ_SOURCE,
        ),
        InstallDir(
            "LQ/Extras/Morrowind of Darkness",
            RENAME="Textures",
            REQUIRED_USE="texture_size_512 darkness",
            S=LQ_SOURCE,
        ),
        InstallDir(
            "MQ/Extras/Morrowind of Darkness",
            RENAME="Textures",
            REQUIRED_USE="texture_size_1024 darkness",
            S=MQ_SOURCE,
        ),
        InstallDir(
            "HQ/Extras/Morrowind of Darkness",
            RENAME="Textures",
            REQUIRED_USE="texture_size_2048 darkness",
            S=HQ_SOURCE,
        ),
        InstallDir(
            "HQ/Extras/Morrowind of Darkness",
            RENAME="Textures",
            REQUIRED_USE="texture_size_4096 darkness",
            S=HQ_SOURCE,
        ),
        InstallDir(
            "UHQ (use with HQ)/Extras/Morrowind of Darkness",
            RENAME="Textures",
            REQUIRED_USE="texture_size_4096 darkness",
            S=UHQ_SOURCE,
        ),
    ]
