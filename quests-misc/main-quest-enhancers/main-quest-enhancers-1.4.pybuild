# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import DOWNLOAD_DIR, MW, File, InstallDir


class Package(MW):
    NAME = "Main Quest Enhancers"
    DESC = "Adds a greater 6th House presence to the game as the main quest progresses"
    HOMEPAGE = """
        https://mw.modhistory.com/download--6828
        https://abitoftaste.altervista.org/morrowind/index.php?option=downloads&task=info&id=72
    """
    # Original is attribution, but abot's fix is all-rights-reserved
    LICENSE = "attribution all-rights-reserved"
    RESTRICT = "fetch"
    RDEPEND = """
        base/morrowind[tribunal]
        !!base/morrowind-rebirth
    """
    KEYWORDS = "openmw"
    SRC_URI = "MQE_MainQuestEnhancers.7z"

    INSTALL_DIRS = [
        InstallDir("Data Files", PLUGINS=[File("MQE_MainQuestEnhancers.esp")])
    ]

    def pkg_nofetch(self):
        print("Please download the following file from the url at the bottom")
        print(
            "before continuing and move them to one of the following download directories:"
        )
        print(f"  {DOWNLOAD_DIR}")
        print("  " + os.path.expanduser("~/Downloads"))
        print()
        for source in self.A:
            print(f"  {source}")
        print()
        print(
            "  https://abitoftaste.altervista.org/morrowind/index.php"
            "?option=downloads&task=info&id=72"
        )
        print()
        self.warn(
            "If you're upgrading from an older version of main-quest-enhancers, "
            "note that the filename has not changed, but the file has."
        )
