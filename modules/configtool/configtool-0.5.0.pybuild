# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil

from pybuild.info import PN, PV, P

from common.distutils import Distutils


class Package(Distutils):
    NAME = "Portmod OpenMW Config Module"
    DESC = "Sorts openmw.cfg and settings.cfg to match mods installed by portmod."
    LICENSE = "GPL-3"
    KEYWORDS = "openmw tes3mp"
    HOMEPAGE = f"https://gitlab.com/portmod/{PN}"
    SRC_URI = f"https://gitlab.com/portmod/{PN}/-/archive/{PV}/{P}.tar.gz"
    S = f"{P}/{P}"
    IUSE = "grass"
    RDEPEND = "dev-python/configobj"

    def src_prepare(self):
        super().src_prepare()
        if "grass" in self.USE:
            self.SETTINGS = {
                "Groundcover": {
                    "enabled": "true",
                    "density": 0.5,
                    "min chunk size": 0.5,
                }
            }

    def src_install(self):
        os.makedirs(os.path.join(self.D, "modules"))
        shutil.move(
            "openmw-config.pmodule",
            os.path.join(self.D, "modules", "openmw-config.pmodule"),
        )
        super().src_install()
