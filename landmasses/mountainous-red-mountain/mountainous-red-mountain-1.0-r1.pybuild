# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import PV

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Mountainous Red Mountain"
    DESC = "Increases the height of Red Mountain and makes the landscape a more spikey"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/42125
        https://mw.modhistory.com/download--15169
    """
    LICENSE = "all-rights-reserved"
    RESTRICT = "mirror"
    # Conflicts with Morrowind Rebirth.
    # See https://forum.openmw.org/viewtopic.php?f=40&p=65276#p65276
    RDEPEND = """
        base/morrowind
        land-rocks/on-the-rocks[red-mountain]
        land-flora/ashlands-overhaul
        !!base/morrowind-rebirth
    """
    KEYWORDS = "~openmw"
    SRC_URI = f"""
        https://mw.modhistory.com/file.php?id=15004 -> MRM-{PV}.7z
        https://mw.modhistory.com/file.php?id=15169 -> MRM_PuzzleCanal_Fix_v1.1.7z
        non-pointy? (
            https://www.dropbox.com/s/6rakhzt9c0d7epx/MRM-Non%20Pointy.rar?dl=1
            -> MRM-Non-Pointy.rar
        )
    """
    IUSE = "+non-pointy"
    TEXTURE_SIZES = "512"
    INSTALL_DIRS = [
        InstallDir("MRM/Data Files", PLUGINS=[File("MRM.esm")], S=f"MRM-{PV}"),
        InstallDir(
            ".", PLUGINS=[File("MRM_PuzzleCanal_Fix.esp")], S="MRM_PuzzleCanal_Fix_v1.1"
        ),
        InstallDir(
            "MRM-Non Pointy",
            PLUGINS=[File("MRM-Non Pointy.esm")],
            # Appears to be an old patch. GDR has a patch that is much newer than this.
            BLACKLIST=["DN-GDR BTB MRM-Non Pointy.esp"],
            S="MRM-Non-Pointy",
        ),
    ]
