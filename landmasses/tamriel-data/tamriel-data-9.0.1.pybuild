# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod

VANILLA_FILE = "Tamriel_Data_(Vanilla)-44537-9-0-1-1668999176"
HD_FILE = "Tamriel_Data_(HD)-44537-9-0-1-1668999061"


class Package(NexusMod, MW):
    NAME = "Tamriel Data"
    DESC = "Adds game data files required by several landmass addition mods"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org
        https://www.nexusmods.com/morrowind/mods/44537
    """
    # License in 8 is the same as in 7.1
    LICENSE = "tamriel-data-7.1"
    # weapon-sheathing-tr is included
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        !!gameplay-weapons/weapon-sheathing-tr
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        texture_size_512? ( {VANILLA_FILE}.7z )
        devtools? ( Tamriel_Data_Legacy-44537-06-01.7z )
        texture_size_1024? ( {HD_FILE}.7z )
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/44537"
    IUSE = "devtools"
    RESTRICT = "fetch"
    TIER = 2

    TEXTURE_SIZES = "512 1024"
    INSTALL_DIRS = [
        InstallDir(
            "00 Data Files",
            ARCHIVES=[File("TR_Data.bsa"), File("PC_Data.bsa"), File("Sky_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=VANILLA_FILE,
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            "00 Data Files",
            ARCHIVES=[File("TR_Data.bsa"), File("PC_Data.bsa"), File("Sky_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=HD_FILE,
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                # Note that Tamriel_Data_Legacy should not be enabled in game.
                # This option is included as a development tool.
                # File("Tamriel_Data_Legacy.esm")
            ],
            S="Tamriel_Data_Legacy-44537-06-01",
            REQUIRED_USE="devtools",
        ),
    ]
