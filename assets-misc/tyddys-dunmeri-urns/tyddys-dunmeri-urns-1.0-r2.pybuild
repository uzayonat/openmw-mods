# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.fix_maps import FixMaps
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(FixMaps, NexusMod, MW):
    NAME = "Dunmeri Urns - Aestetika of Vvardenfell"
    DESC = "HQ Retexture and optional replacer for dunmeri urns and planters"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43541"
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43541"
    TEXTURE_SIZES = "1024 2048"
    SRC_URI = """
        texture_size_2048? (
            Urns_HQ-43541-1-0.7z
            map_normal? ( Urns_Bump_Mapped_-_HQ-43541-1-0.7z )
        )
        texture_size_1024? (
            Urns_MQ-43541-1-0.7z
            map_normal? ( Urns_Bump_Mapped_-_MQ-43541-1-0.7z )
        )
    """
    NORMAL_MAP_PATTERN = "_nm"
    IUSE = "map_normal"
    INSTALL_DIRS = [
        InstallDir(
            "HQ/Data Files", S="Urns_HQ-43541-1-0", REQUIRED_USE="texture_size_2048"
        ),
        InstallDir(
            "MQ/Data Files", S="Urns_MQ-43541-1-0", REQUIRED_USE="texture_size_1024"
        ),
        InstallDir(
            "HQ/Data Files",
            S="Urns_Bump_Mapped_-_HQ-43541-1-0",
            REQUIRED_USE="texture_size_2048 map_normal",
        ),
        InstallDir(
            "MQ/Data Files",
            S="Urns_Bump_Mapped_-_MQ-43541-1-0",
            REQUIRED_USE="texture_size_1024 map_normal",
        ),
    ]
