# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.atlasgen import AtlasGen
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(AtlasGen, NexusMod, MW):
    NAME = "Project Atlas"
    DESC = "Texture atlases to improve performance"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45399"
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        glowing-bitter-coast? ( assets-misc/glowing-bitter-coast )
        !glowing-bitter-coast? ( !!assets-misc/glowing-bitter-coast )
        gitd? ( arch-misc/glow-in-the-dahrk )
    """
    DEPEND = "virtual/imagemagick"
    KEYWORDS = "openmw"
    S = "Project_Atlas-45399-0-6-4-1579483867"
    SRC_URI = f"{S}.7z"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45399"
    IUSE = "gitd smooth glowing-bitter-coast"
    DATA_OVERRIDES = "assets-misc/morrowind-optimization-patch"
    TEXTURE_SIZES = "512 1024"
    TIER = "z"

    INSTALL_DIRS = [
        InstallDir(
            "00 Core",
            ATLASGEN=[
                File("dragonstatue_atlas_generator.bat"),
                File("emperor_parasol_atlas_generator.bat"),
                File("hlaalu_atlas_generator.bat"),
                File("imperial_atlas_generator.bat"),
                File("nord_atlas_generator.bat"),
                File("nordcommon_atlas_generator.bat"),
                File("redoran_atlas_generator.bat"),
                File("redware_atlas_generator.bat"),
                File("redware_pot_atlas_generator.bat"),
                File("urn_atlas_generator.bat"),
                File("velothi_atlas_generator.bat"),
                File("woodpoles_atlas_generator.bat"),
                File("bc_mushrooms_atlas_generator.bat"),
            ],
        ),
        InstallDir("10 Glow in the Dahrk Patch", REQUIRED_USE="gitd"),
        InstallDir(
            "20 BC Mushrooms - Smoothed",
            ATLASGEN=[
                File("bc_fungus_atlas_generator.bat"),
                File("bc_mushroom_only_atlas_generator.bat"),
            ],
            REQUIRED_USE="smooth",
        ),
        InstallDir(
            "20 BC Mushrooms - Normal - Glowing Bitter Coast Patch",
            REQUIRED_USE="!smooth glowing-bitter-coast",
        ),
        InstallDir(
            "20 BC Mushrooms - Smoothed - Glowing Bitter Coast Patch",
            REQUIRED_USE="smooth glowing-bitter-coast",
        ),
        InstallDir("30 Redware - Smoothed", REQUIRED_USE="smooth"),
        InstallDir("40 Urns - Smoothed", REQUIRED_USE="smooth"),
        InstallDir("50 Wood Poles - Hi-Res Texture", REQUIRED_USE="texture_size_1024"),
    ]
