# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from typing import Optional

from pybuild import Pybuild2

from common.mw import DOWNLOAD_DIR, MW, use_reduce


class Package(Pybuild2):
    NAME = "Nexus"
    DESC = "Boilerplate for prompting the user to download files from nexusmods.com"
    KEYWORDS = "openmw tes3mp"


class NexusMod(MW):
    """
    Pybuild Class for mods that are only available through NexusMods
    If NEXUS_URL is defined, automatically implements pkg_nofetch
    """

    """
    Path of the nexus page for this package

    May include multiple links as well as use-conditionals

    If NEXUS_SRC_URI is not specified, users will be directed to download missing
    files in SRC_URI from these links.

    This should only be omitted if NEXUS_SRC_URI is used and NexusMods is not the
    primary download location.
    """
    NEXUS_URL: Optional[str]
    """
    Nexus file download page links, mapped to file names, using an arrow operator.

    Note that if the package is not redistributable, the file names should match the
    file name on NexusMods (with spaces replaced with underscores), as the user must
    still download them manually.

    E.g.
        https://www.nexusmods.com/<game>/mods/<id>?tab=files&file_id=<file_id>
        -> FileName.ext
    """
    NEXUS_SRC_URI: Optional[str]

    def __init__(self):
        if self.NEXUS_SRC_URI:
            for token in self.NEXUS_SRC_URI.split():
                # Extend SRC_URI using NEXUS_SRC_URI, omitting the URLs and Arrows,
                # so that only use_conditionals and file names are retained.
                if (
                    not token.startswith("https://")
                    and not token.startswith("http://")
                    and not token == "->"
                ):
                    self.SRC_URI += f" {token}"

    def parse_nexus_src_uri(self):
        results = use_reduce(self.NEXUS_SRC_URI, self.USE, is_src_uri=True, flat=True)
        for url, arrow, file in list(zip(results[::3], results[1::3], results[2::3])):
            if arrow != "->":
                raise Exception(f"Malformed NEXUS_SRC_URI: {self.NEXUS_SRC_URI}")
            yield url, file

    def pkg_nofetch(self):
        super().pkg_nofetch()
        if self.NEXUS_SRC_URI or self.NEXUS_URL:
            print("Please download the following files before continuing")
            print("and move them to one of the following download directories:")
            print(f"  {DOWNLOAD_DIR}")
            print("  " + os.path.expanduser("~/Downloads"))
            print()
        if self.NEXUS_SRC_URI:
            unfetched = {source.name for source in self.UNFETCHED}
            for url, source in self.parse_nexus_src_uri():
                if source in unfetched:
                    print(f"  {source}")
                    print(f"  {url}")
                    print()
        elif self.NEXUS_URL:
            for source in self.UNFETCHED:
                if os.path.basename(source.url) == source.url:
                    print(f"  {source.name}")
            print()
            for url in use_reduce(self.NEXUS_URL, self.USE):
                print(f"  {url}?tab=files")
