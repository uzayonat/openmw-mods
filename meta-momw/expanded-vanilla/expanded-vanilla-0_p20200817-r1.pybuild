# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW


class Package(MW):
    NAME = "Expanded Vanilla Metamod"
    DESC = (
        "A collection that tries to preserve the look and feel of the vanilla game "
        "while adding a decent amount of new content"
    )
    HOMEPAGE = "https://modding-openmw.com/mods/tag/expanded-vanilla/"
    LICENSE = "CC-BY-SA-4.0"
    KEYWORDS = "openmw"

    # Missing:
    # media-audio/resdayn-suite (requires purchase.
    #   anyone with a copy is free to create a pybuild)
    RDEPEND = """
        base/morrowind
        assets-meshes/mesh-fix
        assets-meshes/correct-meshes
        assets-meshes/correct-uv-mudcrabs
        assets-meshes/correct-uv-rocks
        base/patch-for-purists
        meta-misc/official-plugins
        gameplay-misc/expansion-delay
        gameplay-misc/container-ownership
        quests-misc/dubdilla-location-fix
        landmasses/tamriel-rebuilt
        landmasses/skyrim-home-of-the-nords
        landmasses/province-cyrodiil
        assets-meshes/properly-smoothed-meshes
        assets-misc/morrowind-optimization-patch
        arch-misc/glow-in-the-dahrk
        gameplay-misc/graphic-herbalism
        assets-textures/intelligent-textures
        assets-textures/facelift
        arch-towns/distant-mournhold
        assets-misc/project-atlas
        assets-misc/dunmer-lanterns-replacer
        items-misc/improved-better-skulls
        assets-misc/pherim-bread
        assets-misc/pherim-guar-cart
        assets-misc/pherim-cavern-clutter
        assets-misc/bloodmoon-hide-replacer
        gameplay-misc/book-rotate
        items-misc/dagoth-ur-welcomes-you
        items-misc/illuminated-palace-of-vivec
        gameplay-misc/containers-animated
        items-misc/redoran-council-hall-improvement
        npcs-misc/yet-another-guard-diversity
        npcs-bodies/animation-compilation
        assets-textures/concept-art-vivec-face-replacement
        npcs-voices/dagoth-ur-voice-addon
        npcs-voices/almalexia-voice
        npcs-bodies/dirnaes-beast-animations
        npcs-bodies/almalexias-cast-for-beasts
        npcs-misc/starfires-npcs
        npcs-misc/nastier-camonna-tong
        npcs-misc/umbra-blademaster
        npcs-voices/vivec-voice-addon
        items-weapons/correct-iron-warhammer
        creatures-misc/blighted-animals-retextured
        arch-towns/balmora-underworld
        quests-misc/fetchers-of-morrowind
        quests-misc/immersive-imperial-skirt
        gameplay-misc/persistent-corpse-disposal
        npcs-misc/no-more-stage-diving
        gameplay-misc/portable-homes
        npcs-misc/greetings-for-no-lore
        gameplay-misc/expansions-integrated
        arch-towns/unique-tavern-signs-tr
        arch-misc/ports-of-vvardenfell
        arch-towns/rr-lighthouse-tel-branora
        arch-towns/rr-lighthouse-tel-vos
        arch-misc/tower-of-vos
        arch-misc/stav-gnisis-minaret
        skies/swg-skies
        ui/monochrome-user-interface
        ui/widescreen-alaisiagae-splash-screens
        ui/mlse
        ui/hd-splash-and-menu
        arch-player/abandoned-flat
        arch-player/fargoths-mountain-hut
        quests-misc/gondoliers-license
        gameplay-misc/comfy-pillow-for-a-restful-sleep
        arch-dungeons/sobitur-facility
        assets-meshes/dwemer-mesh-improvement
        arch-dungeons/face-of-the-hortator
        assets-meshes/fix-those-bastard-rope-fences
        gameplay-misc/improved-shrine-journal-entries
        assets-textures/petes-journal
        quests-factions/rise-of-house-telvanni
        quests-misc/rise-of-the-tribe-unmourned
        quests-misc/main-quest-enhancers
        creatures-misc/triangletooth-ecology
        items-misc/adventurers-backpacks
        gameplay-misc/caldera-apparatus
        gameplay-misc/at-home-alchemy
        gameplay-weapons/better-fatigue-usage
        gameplay-stats/better-regen
        media-audio/better-sounds
        gameplay-advancement/sensible-races-and-birthsigns
        factions/beware-the-sixth-house
        gameplay-misc/tribunal-rebalance
        gameplay-misc/bloodmoon-rebalance
        gameplay-advancement/ncgd
        gameplay-advancement/mbsp
        gameplay-stats/carryon
        gameplay-misc/creeper-mudcrab-no-trade
        gameplay-misc/price-balance
        gameplay-misc/clear-your-name
        quests-misc/illegal-items
        gameplay-misc/dwemer-ebony-service-refusal
        gameplay-misc/faction-service-beds
        gameplay-misc/lower-first-person-sneak-mode
        gameplay-weapons/marksman-overhaul
        gameplay-weapons/projectile-overhaul
        gameplay-misc/go-to-jail
        gameplay-misc/wait-and-sleep
        gameplay-misc/on-the-move
        gameplay-weapons/weapon-sheathing
        gameplay-weapons/auto-ammo-equip
        gameplay-misc/timescale-change
        gameplay-misc/speechcraft-rebalance
        gameplay-magic/quill-of-feyfolken
        quests-misc/welcome-to-the-arena
        arch-dungeons/mines-and-caverns
        npcs-misc/wealth-within-measure
        gameplay-misc/true-lights-and-darkness
        items-misc/glowing-flames
        arch-towns/caldera-mine-expanded
        quests-factions/uviriths-legacy
        quests-factions/building-up-uviriths-legacy
        arch-interiors/vivec-guild-of-mages-expansion
        arch-interiors/wolverine-hall-expansion
        gameplay-magic/multiple-teleport-marking
        gameplay-misc/npc-schedule
        gameplay-misc/nighttime-door-locks
        media-audio/great-service
        ui/hd-texture-buttons
        modules/openmw-fonts
        media-fonts/pelagiad
        media-fonts/ayembedt
        media-fonts/dejavu
        virtual/merged-plugin
    """
