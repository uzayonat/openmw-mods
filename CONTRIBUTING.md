# Contributing

Common contributions include:
- Finding and adding new mods to the repository (and ideally testing them if their stability is unknown).
- Updating mod packages which are present but out of date.

See [Mod Maintainers](https://gitlab.com/portmod/portmod/-/wikis/Mod-Maintainers) if you want to help keep mod packages up to date. This is not mandatory: one-time updates to packages are always welcomed!

## Licensing and Contribution Origins
Contributing to this repository means that you are licensing your contributions under the GPL version 3 or later.

When making contributions, you can optionally agree to the Gentoo Certificate of Origin by adding
```
Signed-off-by: Name <e-mail>
```
to your commit messages (as a separate line). This can be done automatically by committing using the flag `-s`/`--signoff` with `git commit`.

The following is revision 1 of the Gentoo Certificate of Origin:

> By making a contribution to this project, I certify that:
>
>    The contribution was created in whole or in part by me, and I have the right to submit it under the free software license indicated in the file; or
>
>    The contribution is based upon previous work that, to the best of my knowledge, is covered under an appropriate free software license, and I have the right under that license to submit that work with modifications, whether created in whole or in part by me, under the same free software license (unless I am permitted to submit under a different license), as indicated in the file; or
>
>    The contribution is a license text (or a file of similar nature), and verbatim distribution is allowed; or
>
>    The contribution was provided directly to me by some other person who certified 1., 2., 3., or 4., and I have not modified it.
>
>    I understand and agree that this project and the contribution are public and that a record of the contribution (including all personal information I submit with it, including my sign-off) is maintained indefinitely and may be redistributed consistent with this project or the free software license(s) involved.

The Gentoo certificate of Origin is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License, and was originally found [here](https://www.gentoo.org/glep/glep-0076.html#certificate-of-origin) (note that Portmod does not fall within the scope of the Gentoo project; it is just that it is useful for Portmod to make use of some of the Gentoo documentation and resources).

## Copyright Statements
Files must begin with a copyright statement such as included in `header.txt`.

In general, we use the convention that copyright is listed as
```
Copyright YEARS Portmod Authors
```
to refer to all contributors generally. All copyright is retained by the original authors, and specific authorship information can be obtained using git metadata.

Note that only files which have changed since the date given in the copyright statement should have their statement updated to include the current year.

## Getting Started

There's a [guide](https://gitlab.com/portmod/portmod/-/wikis/OpenMW/Package-Guide) which describes the basics of creating pybuild files, and a bunch of other resources on the [wiki](https://gitlab.com/portmod/portmod/-/wikis/OpenMW/OpenMW) and in the [Package Development](https://portmod.readthedocs.io/en/stable/dev/packages.html#package-development) section of the developer guide which explain the details of the system.

There's also a tool called [importmod](https://gitlab.com/portmod/importmod) which can be used to generate partial build files for mods automatically. Note that this tool is somewhat undocumented, and may not be particularly stable.

When you have created a package you would like to submit, you need to fork this repository, create a feature branch and create a merge request to merge your changes into this repository. If you are unfamiliar with this process, see the [GitLab Basics guide](https://docs.gitlab.com/ee/topics/git/), and in particular, [Make your first Git commit](https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html).

## Required Tools

There are a few tools which are used by CI checks, and as such will be necessary for you to make use of if you want your merge request to be accepted.

### Inquisitor

Inquisitor (included with portmod) is used both to validate pybuild files (`inquisitor scan <pybuild>`), and to (re)generate `Manifest` files (`inquisitor manifest <pybuild>`).

### Black

[Black](https://github.com/psf/black) is used to format pybuild files, and ensures both that the files in this repository are formatted consistently, and that updates to files are concise and never include reformatting noise (since they'll be formatted correctly to start with).

### Flake8

[Flake8](https://gitlab.com/pycqa/flake8) enforces python-related QA checks, and should be used ahead of submissions to validate the python code within the pybuild files. Unlike black, flake8 will not fix the problems for you.

## Pre-commit

The checks run by CI can be run locally in an automatic fashion using [pre-commit](https://pre-commit.com/).
This repository has both pre-commit hooks for the tests, and a prepare-commit-msg hook, which will automatically generate commit messages for common operations such as adding new packages, or updating packages.

To get started with pre-commit (once you've installed it), run `pre-commit install -t pre-commit -t prepare-commit-msg` from within your local copy of the repository to install the hooks. Pre-commit will then automatically set up and run the checks when you next create a commit.
