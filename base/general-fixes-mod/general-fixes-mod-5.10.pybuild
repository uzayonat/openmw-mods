# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import json
import os
import re

from chardet import detect

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "General Fixes Mod 5.10 RU 1C"
    DESC = "This mod collects fixes for common, potentially game-breaking bugs"
    HOMEPAGE = "https://tesall.ru/files/file/1749-general-fixes-mod-gfm/"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    KEYWORDS = "openmw"
    TIER = 1
    IUSE = "l10n_ru"
    SRC_URI = "GFM_5.10_1C.7z"
    INSTALL_DIRS = [
        InstallDir(
            "GFM_5.10_1C",
            WHITELIST=["Morrowind.ini", "Angel.ini", "GFM_1C.txt"],
            S=".",
        ),
        InstallDir(
            "GFM_5.10_1C/Data Files",
            PLUGINS=[File("GFM_1C.esm"), File("GMST-Fix_1C.esp")],
            WHITELIST=[
                "BloodMoon.mrk",
                "GFM_1C.esm",
                "GFM_1C.cel",
                "GFM_1C.mrk",
                "GFM_1C.top",
                "GMST-Fix_1C.esp",
                "morrowind.cel",
                "morrowind.top",
            ],
            S=".",
        ),
    ]

    def src_prepare(self):
        self.read_ini(os.path.join(self.WORKDIR, "GFM_5.10_1C", "Morrowind.ini"))

    def read_ini(self, path):
        """
        This mod has updated version of Morrowind.ini,
        so we load it same as base/morrowind does
        """

        section = None
        self.FALLBACK = {}
        with open(os.path.join(self.FILESDIR, "ini_whitelist.json"), "r") as file:
            whitelist_strings = set(json.load(file))

        with open(os.path.join(self.FILESDIR, "ini_regexes.json"), "r") as file:
            whitelist_regexes = [re.compile(s, re.I) for s in json.load(file)]

        with open(path, "rb") as file:
            result = detect(file.read())

        if result["confidence"] < 0.95:
            self.warn(
                f'Detected {result["encoding"]} for Morrowind.ini with confidence of '
                f'only {result["confidence"]}\n'
                "You should double-check that this encoding is correct and that the "
                "fallback entries in opennw.cfg are not garbled."
            )

        with open(path, "r", encoding=result["encoding"]) as config:
            for line in config.readlines():
                if re.match(r"\s*^\[.*\]\s*$", line):
                    section = line.strip().strip("[]")
                    self.FALLBACK[section] = {}
                elif "=" in line:
                    key = line.split("=", 1)[0].strip()
                    value = line.split("=", 1)[1].strip()
                    entry_name = f"{section}:{key}"
                    if entry_name in whitelist_strings or any(
                        regex.match(entry_name) for regex in whitelist_regexes
                    ):
                        self.FALLBACK[section][key] = value
