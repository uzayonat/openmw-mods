# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.fix_maps import FixMaps
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, FixMaps, MW):
    NAME = "Mines and Caverns"
    DESC = "This mod improves and/or expands several vanilla dungeons"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/44893"
    KEYWORDS = "openmw"
    # Permissions section on NexusMods says that you can redistribute,
    # however this mod bundles a lot of content from other mods, and while
    # attribution is included, it's not clear if permission was obtained,
    # and some included mods do not have a license that provides derivation
    # rights (or even licenses at all in some cases).
    LICENSE = "all-rights-reserved"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/44893"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
    """
    IUSE = "devtools"
    # Not compatible with bloated caves
    # Readme states that it's not compatible with gameplay-misc/true-lights-and-darkness
    # however this is apparently just due to it not affecting the new cells, so it is
    # not a significant enough conflict to require a blocker
    MAIN_FILE = "Mines_and_Caverns-44893-6-0-1592335906"
    SRC_URI = f"""
        {MAIN_FILE}.rar
        BlindCaveFish-44893-1-0.rar
    """
    # Mines_and_Caverns-DD-44893-5-5-1575336607.rar
    # MC_Animated_Morrowind_II-44893-5-8-1584822816.rar
    # Julan_Ashlander_MC_patch-44893-1-0-1564857480.rar
    NORMAL_MAP_PATTERN = "_NM"
    INSTALL_DIRS = [
        InstallDir(
            "Mines & Caverns",
            PLUGINS=[File("Clean_Mines & Caverns.esp")],
            S=MAIN_FILE,
        ),
        InstallDir(
            "BlindCaveFish",
            PLUGINS=[File("Clean_BlindCaveFish.esp")],
            S="BlindCaveFish-44893-1-0",
        ),
        # Patch for Julan Ashlander Companion
        # InstallDir(
        #     "KS_Julan_Ashlander - MC_patch",
        #     PLUGINS=[File("Clean_KS_Julan_Ashlander - MC_patch.esp")],
        #     S="Julan_Ashlander_MC_patch-44893-1-0-1564857480",
        # ),
        # Patch for Detailed Dungeons
        # InstallDir(
        #     "Detailed Dungeons",
        #     PLUGINS=[File("Clean_Detailed Dungeons.esp")],
        #     S="Mines_and_Caverns-DD-44893-5-5-1575336607",
        # ),
        # Patch for animated morrowind II
        # InstallDir(
        #     "MC_Animated Morrowind II",
        #     PLUGINS=[File("Animated Morrowind II.esp")],
        #     S="MC_Animated_Morrowind_II-44893-5-8-1584822816",
        # ),
    ]
