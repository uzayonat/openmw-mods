## Summary

(Summarize the encountered bug concisely)

## Steps to Reproduce

(How can this issue be reproduced?)

## Current Behaviour

(Include a stack trace and output log if relevant. You may want to enable the `--verbose` option, which may include more information)

## Expected Behaviour

(If the behaviour is unusual, but not fatal, what is the expected behaviour?)
(Omit if not relevant, such as if it fails completely and crashes portmod)

## Portmod and Prefix Information

(include the output of `portmod <prefix> info` here)

(Also include any other relevant information such as if you're using a version from a branch, or have custom changes not available in the repository)

## Possible fixes

(If you can, link to the line of code which may be responsible for the problem)
(If you know of any changes which can be made to fix this, list them here)

/label ~bug
