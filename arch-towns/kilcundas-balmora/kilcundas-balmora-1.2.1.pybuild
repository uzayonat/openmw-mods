# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod
from common.util import CleanPlugin

MAIN_FILE = "Kilcunda's_Balmora_1.2.1_(Main_File)-44149-1-2-1"
FIX_FILE = "North-East_Balmora_Seam_Fix-44149-1-0-1557026152"


class Package(CleanPlugin, NexusMod, MW):
    NAME = "Kilcunda's Balmora"
    DESC = "Gives Balmora a fresh look by adding more to it but without going overboard"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/44149"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    DEPEND = RDEPEND
    KEYWORDS = "openmw"
    SRC_URI = f"""
        {MAIN_FILE}.zip
        {FIX_FILE}.rar
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/44149"
    TEXTURE_SIZES = "1024"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[File("Kilcunda's Balmora.ESP")],
            S="Kilcunda's_Balmora_1.2.1_(Main_File)-44149-1-2-1",
        ),
        InstallDir(
            "Data",
            PLUGINS=[File("Kilcunda's Balmora_SeamFix.ESP")],
            S="North-East_Balmora_Seam_Fix-44149-1-0-1557026152",
        ),
    ]

    def src_prepare(self):
        self.clean_plugin("Data Files/Kilcunda's Balmora.ESP")
        self.clean_plugin(
            f"{self.WORKDIR}/{FIX_FILE}/Data/Kilcunda's Balmora_SeamFix.ESP"
        )
