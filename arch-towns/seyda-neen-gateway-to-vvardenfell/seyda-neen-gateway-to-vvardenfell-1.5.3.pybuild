# Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    DESC = "Vanilla friendly overhaul of Seyda Neen"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/48781"
    KEYWORDS = "openmw"
    LICENSE = "mw-by-nc"
    NAME = "Seyda Neen - Gateway to Vvardenfell"
    IUSE = "taller-vanilla no-edits"
    REQUIRED_USE = "^^ ( taller-vanilla no-edits )"
    RDEPEND = """
       base/morrowind[bloodmoon,tribunal]
    """
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/48781?tab=files&file_id=1000023709 -> Seyda_Neen_-_Gateway-48781-1-5-3-1617470635.7z
        https://www.nexusmods.com/morrowind/mods/48781?tab=files&file_id=1000021523 -> Seyda_Neen_-_Gateway_-_No_Lighthouse_Edits-48781-1-5-1-1603998881.7z
        https://www.nexusmods.com/morrowind/mods/48781?tab=files&file_id=1000021524 -> Seyda_Neen_-_Gateway_-_Vanilla_Lighthouse_but_taller-48781-1-5-1-1603998917.7z
    """
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="Seyda_Neen_-_Gateway-48781-1-5-3-1617470635",
            PLUGINS=[File("Seyda_Neen_Gateway.ESP")],
            REQUIRED_USE="!taller-vanilla !no-edits",
        ),
        InstallDir(
            "00 Core",
            S="Seyda_Neen_-_Gateway_-_No_Lighthouse_Edits-48781-1-5-1-1603998881",
            PLUGINS=[File("Seyda_Neen_Gateway_VanillaLighthouse.ESP")],
            REQUIRED_USE="no-edits",
        ),
        InstallDir(
            "00 Core",
            S="Seyda_Neen_-_Gateway_-_Vanilla_Lighthouse_but_taller-48781-1-5-1-1603998917",
            PLUGINS=[File("Seyda_Neen_Gateway_VanillaLighthouse_taller.ESP")],
            REQUIRED_USE="taller-vanilla",
        ),
    ]
