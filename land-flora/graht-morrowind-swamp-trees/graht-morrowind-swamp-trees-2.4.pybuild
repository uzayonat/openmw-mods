# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Graht Morrowind Swamp Trees"
    DESC = "Adds enormous trees to the Bitter Coast region to provide a dense canopy overheard and large roots jutting out of the ground below."
    HOMEPAGE = "https://mw.moddinghall.com/file/69-graht-morrowind-swamp-trees/"
    KEYWORDS = "openmw"
    LICENSE = "attribution-derivation-nc"
    IUSE = "+cawumaloe-manor tr twin-lamps-and-slave-hunters +vanilla-trees"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/49771"
    RDEPEND = """
        base/morrowind
        tr? ( landmasses/tamriel-rebuilt )
        twin-lamps-and-slave-hunters? ( ~factions/twin-lamps-and-slave-hunters-2.0.7b )
    """
    SRC_URI = """
        https://mw.moddinghall.com/file/69-graht-morrowind-swamp-trees/?do=download -> Graht_Morrowind_Swamp_Trees-49771-2-4-1649099371.7z
    """
    INSTALL_DIRS = [
        InstallDir(
            "00 Core",
            PLUGINS=[File("Graht Morrowind Swamp Trees.ESP")],
        ),
        InstallDir("01 Matching Vanilla Trees", REQUIRED_USE="vanilla-trees"),
        # InstallDir "02 Vality BC Patch" is configured directly in land-flora/vality-bitter-coast
        InstallDir(
            "03 Cawumaloe Manor",
            PLUGINS=[
                File(
                    "GMST Cawumaloe Manor.esp",
                    OVERRIDES="Graht Morrowind Swamp Trees.ESP",
                )
            ],
            REQUIRED_USE="cawumaloe-manor",
        ),
        InstallDir(
            "04 Tamriel Rebuilt Sundered Scar Region",
            PLUGINS=[File("GMST Sunder Scar Region.ESP")],
            REQUIRED_USE="tr",
        ),
        InstallDir(
            "05 Nevena's Twin Lamps Patch",
            DATA_OVERRIDES="factions/twin-lamps-and-slave-hunters",
            PLUGINS=[File("GMST Nevena's Twin Lamps & Slave Hunters_Edited.esp")],
            REQUIRED_USE="twin-lamps-and-slave-hunters",
        ),
    ]
